package org.gcube.accounting.analytics;

import java.util.Calendar;
import java.util.SortedMap;

import org.gcube.accounting.analytics.TemporalConstraint.AggregationMode;
import org.gcube.accounting.analytics.exception.DuplicatedKeyFilterException;
import org.gcube.accounting.analytics.exception.KeyException;
import org.gcube.accounting.analytics.exception.ValueException;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceBackendQuery;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceBackendQueryFactory;
import org.gcube.accounting.datamodel.aggregation.AggregatedStorageStatusRecord;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountingPersistenceBackendQueryTest {

	private static Logger logger = LoggerFactory.getLogger(AccountingPersistenceBackendQueryTest.class);
	
	@Test
	public void testStorageTimeSeries() throws DuplicatedKeyFilterException, KeyException, ValueException, Exception {
		AccountingPersistenceBackendQuery apbq = AccountingPersistenceBackendQueryFactory.getInstance();
		
		Calendar startTimeCalendar = Calendar.getInstance();
		startTimeCalendar.set(Calendar.YEAR, 2021);
		startTimeCalendar.set(Calendar.MONTH, Calendar.OCTOBER);
		startTimeCalendar.set(Calendar.DAY_OF_MONTH, 1);
		startTimeCalendar.set(Calendar.HOUR_OF_DAY, 0);
		startTimeCalendar.set(Calendar.MINUTE, 0);
		
		Calendar entTimeCalendar = Calendar.getInstance();
		entTimeCalendar.set(Calendar.YEAR, 2021);
		entTimeCalendar.set(Calendar.MONTH, Calendar.NOVEMBER);
		entTimeCalendar.set(Calendar.DAY_OF_MONTH, 22);
		entTimeCalendar.set(Calendar.HOUR_OF_DAY, 0);
		entTimeCalendar.set(Calendar.MINUTE, 0);
		
		TemporalConstraint temporalConstraint = new TemporalConstraint(startTimeCalendar.getTimeInMillis(), entTimeCalendar.getTimeInMillis(), AggregationMode.DAILY);
		
		apbq.setRequestedRecords(AggregatedStorageStatusRecord.class);
		apbq.setTemporalConstraint(temporalConstraint);
			
		SortedMap<Calendar, Info> timeseries = apbq.getTimeSeries();
		for(Calendar c : timeseries.keySet()) {
			Info info = timeseries.get(c);
			logger.debug("{}", info);
		}
		
	}
	
}
