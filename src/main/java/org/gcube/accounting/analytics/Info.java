/**
 * 
 */
package org.gcube.accounting.analytics;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;


/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class Info {

	public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS Z";
	
	public static DateFormat getDateFormat() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATETIME_PATTERN);
		simpleDateFormat.setTimeZone(TemporalConstraint.DEFAULT_TIME_ZONE);
		return simpleDateFormat;
	}
	
	public static String format(Calendar calendar) {
		return getDateFormat().format(calendar.getTime());
	}
	
	protected Calendar calendar;
	protected JsonNode value;
	
	/**
	 * @param calendar the date
	 * @param value the JSON value
	 */
	public Info(Calendar calendar, JsonNode value) {
		super();
		this.calendar = calendar;
		this.value = value;
	}

	/**
	 * @return the date
	 */
	public Calendar getCalendar() {
		return calendar;
	}

	/**
	 * @param calendar the date to set
	 */
	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}

	/**
	 * @return the value
	 */
	public JsonNode getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(JsonNode value) {
		this.value = value;
	}
	
	@Override
	public String toString(){
		return String.format("Date : %s, Value : %s", format(calendar), value.toString());
	}
}
