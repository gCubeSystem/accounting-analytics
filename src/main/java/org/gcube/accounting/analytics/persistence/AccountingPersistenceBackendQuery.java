/**
 * 
 */
package org.gcube.accounting.analytics.persistence;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;

import org.gcube.accounting.analytics.Filter;
import org.gcube.accounting.analytics.Info;
import org.gcube.accounting.analytics.NumberedFilter;
import org.gcube.accounting.analytics.TemporalConstraint;
import org.gcube.accounting.analytics.UsageValue;
import org.gcube.accounting.datamodel.BasicUsageRecord;
import org.gcube.documentstore.records.AggregatedRecord;
import org.gcube.documentstore.records.Record;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface AccountingPersistenceBackendQuery {

	public static final int KEY_VALUES_LIMIT = 25;

	public static String getScopeToQuery() {
		String scope = AccountingPersistenceQueryFactory.getForcedQueryScope().get();
		if(scope == null) {
			scope = BasicUsageRecord.getContextFromToken();
		}
		return scope;
	}
	
	public void prepareConnection(
			AccountingPersistenceBackendQueryConfiguration configuration)
					throws Exception;

	/** 
	 * @param clz the Usage Record Class of interest
	 * 
	 * Required
	 * 
	 */
	public void setRequestedRecords(Class<? extends AggregatedRecord<?, ?>> clz);
	
	/**
	 * @param temporalConstraint the TemporalConstraint (interval and aggregation)
	 * 
	 * Required
	 */
	public void setTemporalConstraint(TemporalConstraint temporalConstraint);
	
	/**
	 * @param contexts the list of context to use as filter
	 * 
	 * If null or empty list get all data for the interested Record Class 
	 * with the applying temporal constraint with no contexts constraint.
	 * The contexts are evaluated in OR. 
	 *
	 */
	public void setContexts(Set<String> contexts);
	
	/**
	 * @param filters list of filter to obtain the results. 
	 * If null or empty list get all data for the interested Record Class 
	 * with the applying temporal constraint. 
	 * All Filter must have not null and not empty key and value. 
	 * The filters must be related to different keys and are in AND. 
	 * If the list contains more than one filter with the same key an Exception is thrown 
	 * when trying to query.
	 */
	public void setFilters(Collection<? extends Filter> filters);
	
	/**
	 * Query the persistence obtaining a Map where the date is the key and the
	 * #Info is the value. The result is relative to an Usage Record Type,
	 * respect a TemporalConstraint and can be applied one or more filters.
	 * 
	 * @return the Map containing for each date in the required interval the requested data
	 * @throws Exception
	 */
	public SortedMap<Calendar, Info> getTimeSeries() throws Exception;
	
	/**
	 * Return a SortedMap containing the TimeSeries for each context.
	 * @return a SortedMap containing the TimeSeries for each context.
	 */
	public SortedMap<Filter, SortedMap<Calendar, Info>> getContextTimeSeries() throws Exception;
	
	/**
	 * Return a SortedMap containing the TimeSeries for top values for a certain
	 * key taking in account all Filters. 
	 * 
	 * The values are ordered from the most occurred value.
	 * 
	 * @param topKey the key to obtain top values
	 * @param orderingProperty
	 * @return a SortedMap

	 * @throws Exception
	 */
	public SortedMap<NumberedFilter, SortedMap<Calendar, Info>> getTopValues(String topKey, String orderingProperty, Integer limit) throws  Exception;
	
	/**
	 * Close the connection to persistence
	 * 
	 * @throws Exception
	 *             if the close fails
	 */
	public void close() throws Exception;

	public SortedSet<NumberedFilter> getFilterValues(String key) throws Exception;

	public SortedSet<NumberedFilter> getFilterValues(String key, Integer limit) throws Exception;
	
	public Record getRecord(String recordId, String type) throws Exception;

	public SortedMap<Filter, SortedMap<Calendar, Info>> getSpaceTimeSeries(Set<String> dataTypes) throws Exception;

	/**
	 * getUsageValueQuotaTotal
	 *
	 * Example to require 2 different quota (lucio.lelii for service and alessandro.pieve for storage)
	 *	Input:
	 *	[
	 *		TotalFilters [
	 *			clz=class org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord, 
	 *			temporalConstraint=StartTime : 2015-05-01 11:42:34:515 UTC (1430480554515 millis), EndTime : 2016-11-09 11:42:34:515 UTC (1478691754515 millis), 	
	 *			Aggregated DAILY, 
	 *			totalFilters=[
	 *				Filters [filters=[
	 *							{ "consumerId" : "lucio.lelii" }, 
	 *							{ "serviceClass" : "DataAccess" }, 
	 *							{ "serviceName" : "CkanConnector" }
	 *						], d=null, orderingProperty=null], 
	 *				Filters [filters=[
	 *							{ "consumerId" : "lucio.lelii" }, 
	 *							{ "serviceClass" : "VREManagement" }
	 *						], d=null, orderingProperty=null]
	 *			], d=null, orderingProperty=null] 
	 *	]
	 *	Output:
	 *	[
	 *		TotalFilters [
	 *			clz=class org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord,
	 *			temporalConstraint=StartTime : 2015-05-01 11:42:34:515 UTC (1430480554515 millis), EndTime : 2016-11-09 11:42:34:515 UTC (1478691754515 millis), 
	 *			Aggregated DAILY, 
	 *			totalFilters=[
	 *				Filters [filters=[
	 *							{ "consumerId" : "lucio.lelii" },
	 *							{ "serviceClass" : "DataAccess" }, 
	 *							{ "serviceName" : "CkanConnector" }
	 *						], d=1.0, orderingProperty=operationCount], 
	 *				Filters [filters=[
	 *							{ "consumerId" : "lucio.lelii" }, 
	 *							{ "serviceClass" : "VREManagement" }
	 *						], d=1.0, orderingProperty=operationCount]
	 *			], d=2.0, orderingProperty=null]
	 *	]
	 * @param listUsage
	 * @return List<UsageValue>
	 * @throws Exception
	 */
	public List<UsageValue> getUsageValueQuotaTotal(List<UsageValue> listUsage)
			throws Exception;

	boolean isConnectionActive() throws Exception;

}