This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Accounting Analytics

## [v4.0.0]

- Supporting zero or multiple contexts in all queries [#21353]
- Query parameters are now provided via setter and not as functions parameters [#21353]
- Removed getSpaceProvidersIds() and added getDataType() [#21353]

## [v3.0.1]

- Aligned model

## [v3.0.0]

- Switched JSON management to gcube-jackson [#19115]
- Fixed distro files and pom according to new release procedure


## [v2.8.0] [r4.10.0] - 2018-02-15

- Added the possibility to specify the context to query [#10188]


## [v2.7.0] [r4.8.0] - 2017-12-06

- Allowing to specify a limit when querying values for suggestion [#9943]


## [v2.6.0] [r4.7.0] - 2017-10-09

- Implemented isConnectionActive() API


## [v2.5.0] [r4.5.0] - 2017-06-07

- Added API to sum StorageUsage values got from MapReduce avoiding the client to perform such a computation.


## [v2.4.0] [r4.3.0] - 2016-03-16

- Different management of query filters


## [v2.3.0] [r4.2.0] - 2016-12-15

- Added APIs to support quota service.


## [v2.2.0] [r4.1.0] - 2016-11-07

- Improved the way the library return the querable key for a specific Usage Record


## [v2.1.0] [r4.0.0] - 2016-07-28

- Added getUsageValue() API


## [v2.0.0] [r3.11.0] - 2016-05-18

- Reorganized APIs [#2253]
- Added new API to get top values


## [v1.2.0] [r3.10.1] - 2016-04-08

- Ready for ScopeProvider Removal [#2194]


## [v1.1.0] [r3.10.0] - 2016-02-08

- Internal Changes due to accounting-lib generalization [#1746]


## [v1.0.0] [r3.9.0] - 2015-12-09

- First Release

